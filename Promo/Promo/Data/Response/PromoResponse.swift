//
//  PromoResponse.swift
//  Promo
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation

// MARK: - PromoResponse
struct PromoResponse: Decodable {
    let promos: [Promo]?

    enum CodingKeys: String, CodingKey {
        case promos = "promos"
    }
    
    // MARK: - Promo
    struct Promo: Codable {
        let id: Int?
        let name: String?
        let imagesURL: String?
        let detail: String?

        enum CodingKeys: String, CodingKey {
            case id = "id"
            case name = "name"
            case imagesURL = "images_url"
            case detail = "detail"
        }
    }
}

