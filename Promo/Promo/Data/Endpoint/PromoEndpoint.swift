//
//  PromoEndpoint.swift
//  Promo
//
//  Created by Rizal Hidayat on 31/03/24.
//

import Foundation
import Core

enum PromoEndpoint: String {
    case listPromo = "LIST_PROMO"
    
    var url: URL? {
        guard let path = Constants.promoBundle?.path(forResource: String(describing: PromoEndpoint.self), ofType: "plist"),
              let root = NSDictionary(contentsOfFile: path) as? [String: String],
              let stringUrl = root[self.rawValue],
              let url = URL(string: stringUrl) else {
            return nil
        }
        
        return url
    }
}
