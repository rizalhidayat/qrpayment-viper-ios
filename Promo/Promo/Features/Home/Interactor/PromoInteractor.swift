//
//  PromoInteractor.swift
//  Promo
//
//  Created by Rizal Hidayat on 31/03/24.
//

import Foundation
import Combine
import Core

protocol PromoInteractorProtocol {
    func fetchPromos() -> AnyPublisher<PromoResponse?, Error>
}

class PromoInteractor: PromoInteractorProtocol {
    func fetchPromos() -> AnyPublisher<PromoResponse?, Error> {
        guard let url = PromoEndpoint.listPromo.url else {
            return Fail(error: URLError(.badURL)).eraseToAnyPublisher()
        }
        
        return NetworkManager.shared.execute(url, method: .get)
    }
    
}
