//
//  PromoRouter.swift
//  Promo
//
//  Created by Rizal Hidayat on 30/03/24.
//

import UIKit
import Core
import WebKit

protocol PromoRouterProtocol {
    func navigateToDetail(with url: URL)
}

class PromoRouter {
    private weak var view: PromoViewController?
    
    init(view: PromoViewController?) {
        self.view = view
    }
}

extension PromoRouter: PromoRouterProtocol {
    func navigateToDetail(with url: URL) {
        let vc = PromoDetailViewController(nibName: String(describing: PromoDetailViewController.self) , bundle: Constants.promoBundle)
        let presenter = PromoDetailPresenter(url: url, view: vc)
        vc.presenter = presenter
        vc.title = "Rincian Promo"
        vc.hidesBottomBarWhenPushed = true
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}
