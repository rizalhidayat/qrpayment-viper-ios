//
//  PromoViewController.swift
//  Promo
//
//  Created by Rizal Hidayat on 30/03/24.
//

import UIKit
import Core

protocol PromoViewProtocol {
    func showPromos()
    func showError(message: String)
}

class PromoViewController: UIViewController {

    @IBOutlet weak var promoTableView: UITableView!
    
    var presenter: PromoPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        presenter.loadPromos()
    }
    
    private func initTableView() {
        promoTableView.register(cellClass: PromoTableViewCell.self, bundle: Constants.promoBundle)
        promoTableView.dataSource = self
        promoTableView.delegate = self
    }
}

extension PromoViewController: PromoViewProtocol {
    func showPromos() {
        promoTableView.reloadData()
    }
    
    func showError(message: String) {
        let alertController = UIAlertController(
            title: "Maaf",
            message: message,
            preferredStyle: .alert
        )
        let okayAction = UIAlertAction(title: "Coba Lagi", style: .default) { _ in
            self.presenter.loadPromos()
        }
        alertController.addAction(okayAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
}

extension PromoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = presenter.promos?.count, count > 0 {
            tableView.restore()
            return count
        } else {
            tableView.setEmptyView(title: "Tidak ada promo", message: "Promo tidak tersedia untuk saat ini")
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(cellClass: PromoTableViewCell.self, forIndexPath: indexPath)
        guard let promo = presenter.promos?[indexPath.row] else { return cell }
        cell.configure(with: promo)
        return cell
    }
    
    
}

extension PromoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let urlString = presenter.promos?[indexPath.row].detail, let url = URL(string: urlString) {
            presenter.openPromoDetail(with: url)
        }
    }
}
