//
//  PromoTableViewCell.swift
//  Promo
//
//  Created by Rizal Hidayat on 31/03/24.
//

import UIKit
import Core

class PromoTableViewCell: UITableViewCell {

    @IBOutlet weak var promoImageView: UIImageView!
    @IBOutlet weak var promoNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with promo: PromoResponse.Promo) {
        if let urlString = promo.imagesURL, let url = URL(string: urlString) {
            promoImageView.setImage(with: url)
        }
        promoNameLabel.text = promo.name ?? "-"
    }
    
}
