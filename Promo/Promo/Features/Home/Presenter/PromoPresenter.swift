//
//  PromoPresenter.swift
//  Promo
//
//  Created by Rizal Hidayat on 31/03/24.
//

import Foundation
import Combine

protocol PromoPresenterProtocol {
    var promos: [PromoResponse.Promo]? { get }
    func loadPromos()
    func openPromoDetail(with url: URL)
}

class PromoPresenter: PromoPresenterProtocol {
    private(set) var promos: [PromoResponse.Promo]?
    
    private let view: PromoViewProtocol
    private let interactor: PromoInteractorProtocol
    private let router: PromoRouterProtocol
    
    private var cancellables = Set<AnyCancellable>()
    
    init(view: PromoViewProtocol, interactor: PromoInteractorProtocol, router: PromoRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadPromos() {
        interactor.fetchPromos().receive(on: DispatchQueue.main)
            .sink {
                completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.view.showError(message: error.localizedDescription)
                }
            } receiveValue: { promo in
                self.promos = promo?.promos
                self.view.showPromos()
            }
            .store(in: &cancellables)
        
    }
    
    func openPromoDetail(with url: URL) {
        router.navigateToDetail(with: url)
    }
}
