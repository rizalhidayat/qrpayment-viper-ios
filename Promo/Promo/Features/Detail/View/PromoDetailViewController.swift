//
//  PromoDetailViewController.swift
//  Promo
//
//  Created by Rizal Hidayat on 01/04/24.
//

import UIKit
import WebKit

protocol PromoDetailViewProtocol {
    func showWeb(url: URL)
}

class PromoDetailViewController: UIViewController {
    var presenter: PromoDetailPresenterProtocol!
    
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.loadWeb()
    }

}

extension PromoDetailViewController: PromoDetailViewProtocol {
    func showWeb(url: URL) {
        let request = URLRequest(url: url)
        webView.load(request)
    }
}
