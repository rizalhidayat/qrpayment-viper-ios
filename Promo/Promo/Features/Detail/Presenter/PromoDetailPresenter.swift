//
//  PromoDetailPresenter.swift
//  Promo
//
//  Created by Rizal Hidayat on 01/04/24.
//

import Foundation

protocol PromoDetailPresenterProtocol {
    func loadWeb()
}

class PromoDetailPresenter: PromoDetailPresenterProtocol {
    private let url: URL
    private let view: PromoDetailViewProtocol
    
    init(url: URL, view: PromoDetailViewProtocol) {
        self.url = url
        self.view = view
    }
    
    func loadWeb() {
        view.showWeb(url: url)
    }
}
