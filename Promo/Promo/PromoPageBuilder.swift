//
//  PromoPageBuilder.swift
//  Promo
//
//  Created by Rizal Hidayat on 01/04/24.
//

import UIKit
import Core

public class PromoPageBuilder {
    public static func makePromoPage() -> UIViewController {
        let vc = PromoViewController(nibName: String(describing: PromoViewController.self), bundle: Constants.promoBundle)
        let interactor = PromoInteractor()
        let router = PromoRouter(view: vc)
        let presenter = PromoPresenter(view: vc, interactor: interactor, router: router)
        vc.presenter = presenter
        return vc
    }
}
