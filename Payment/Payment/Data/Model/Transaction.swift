//
//  Transaction.swift
//  Payment
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation

struct Transaction {
    let bankName: String?
    let id: String?
    let price: Int64
    let merchantName: String?
    let createdDate: Date?
}
