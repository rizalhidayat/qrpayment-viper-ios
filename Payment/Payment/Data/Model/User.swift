//
//  User.swift
//  Payment
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation

struct User {
    let id: Int64
    let name: String?
    let balance: Int64
}
