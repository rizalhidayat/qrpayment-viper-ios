//
//  ScannerRouter.swift
//  Payment
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation
import Core

protocol ScannerRouterProtocol {
    func navigateToTransactionConfirmation(transaction: Transaction)
}

class ScannerRouter: ScannerRouterProtocol {
    private weak var view: ScannerViewController?
    
    init(view: ScannerViewController?) {
        self.view = view
    }
    
    func navigateToTransactionConfirmation(transaction: Transaction) {
        let vc = TransactionConfirmationViewController(nibName: String(describing: TransactionConfirmationViewController.self), bundle: Constants.paymentBundle)
        let interactor = TransactionConfirmationInteractor()
        let router = TransactionConfirmationRouter(view: vc)
        let presenter = TransactionConfirmationPresenter(transaction: transaction, view: vc, interactor: interactor, router: router)
        vc.title = "Konfirmasi"
        vc.presenter = presenter
        view?.navigationController?.pushViewController(vc, animated: true)
        
    }
}
