//
//  ScannerPresenter.swift
//  Payment
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation

protocol ScannerPresenterProtocol {
    func readQR(string : String)
}

class ScannerPresenter: ScannerPresenterProtocol {
    private let view: ScannerViewProtocol
    private let router: ScannerRouterProtocol
    
    init(view: ScannerViewProtocol, router: ScannerRouterProtocol) {
        self.view = view
        self.router = router
    }
    
    func readQR(string: String) {
        let components = string.components(separatedBy: ".")
        if components.count != 4 {
            view.showFailedAlert(message: "QR Tidak Valid")
            return
        }
        let bank = components[0]
        let id = components[1]
        let merchant = components[2]
        guard let price = Int64(components[3]) else {
            view.showFailedAlert(message: "QR Tidak Valid")
            return
        }
        let transaction = Transaction(bankName: bank, id: id, price: price, merchantName: merchant, createdDate: nil)
        router.navigateToTransactionConfirmation(transaction: transaction)
    }

}
