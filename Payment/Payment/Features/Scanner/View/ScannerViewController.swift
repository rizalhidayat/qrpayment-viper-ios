//
//  ScannerViewController.swift
//  Payment
//
//  Created by Rizal Hidayat on 30/03/24.
//

import UIKit
import Core
import AVFoundation

protocol ScannerViewProtocol {
    func showFailedAlert(message: String)
}

class ScannerViewController: UIViewController {
    
    @IBOutlet weak var previewCameraView: UIView!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var presenter: ScannerPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCamera()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.global().async {
            self.captureSession.startRunning()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = previewCameraView.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        previewCameraView.layer.addSublayer(previewLayer)
    }
    
    private func initCamera() {
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            print(error)
            return
        }
        
        if captureSession.canAddInput(videoInput) {
            captureSession.addInput(videoInput)
        } else {
            print("Failed to add video input to capture session")
            return
        }
        
        // Configure output
        let metadataOutput = AVCaptureMetadataOutput()
        if captureSession.canAddOutput(metadataOutput) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            print("Failed to add metadata output to capture session")
            return
        }
    }
}

extension ScannerViewController: ScannerViewProtocol {
    func showFailedAlert(message: String) {
        let alertController = UIAlertController(
            title: "Maaf",
            message: message,
            preferredStyle: .alert
        )
        let okayAction = UIAlertAction(title: "Oke", style: .cancel) { _ in
            DispatchQueue.global().async {
                self.captureSession.startRunning()
            }
        }
        alertController.addAction(okayAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension ScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard let metadataObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject else {
            print("Failed to read QR code")
            return
        }
        
        if let qrCodeString = metadataObject.stringValue {
            presenter.readQR(string: qrCodeString)
            captureSession.stopRunning()
        }
    }
}
