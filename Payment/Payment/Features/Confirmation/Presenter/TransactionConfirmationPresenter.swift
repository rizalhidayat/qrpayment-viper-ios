//
//  TransactionConfirmationPresenter.swift
//  Payment
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation

protocol TransactionConfirmationPresenterProtocol {
    func loadCurrentTransaction()
    func backToHome()
    func pay()
}

class TransactionConfirmationPresenter: TransactionConfirmationPresenterProtocol {
    private let transaction: Transaction
    
    private let view: TransactionConfirmationViewProtocol
    private let interactor: TransactionConfirmationInteractorProtocol
    private let router: TransactionConfirmationRouterProtocol
    
    init(transaction: Transaction, view: TransactionConfirmationViewProtocol, interactor: TransactionConfirmationInteractorProtocol, router: TransactionConfirmationRouterProtocol) {
        self.transaction = transaction
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadCurrentTransaction() {
        view.showCurrentTransaction(transaction: transaction)
    }
    
    
    func backToHome() {
        router.popToRoot()
    }
    
    func pay() {
        let currentBalance = interactor.getBalance()
        if currentBalance <= transaction.price {
            view.showFailed(message: "Saldo tidak mencukupi")
            return
        }
        let newBalance = currentBalance - transaction.price
        interactor.updateBalance(to: newBalance)
        interactor.addTransactionHistory(newTransaction: transaction)
        view.showSuccess(message: "Transaksi berhasil")
    }
    
}
