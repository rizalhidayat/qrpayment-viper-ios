//
//  TransactionConfirmationInteractor.swift
//  Payment
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation
import Core

protocol TransactionConfirmationInteractorProtocol {
    func updateBalance(to value: Int64)
    func addTransactionHistory(newTransaction: Transaction)
    func getBalance() -> Int64
}

class TransactionConfirmationInteractor: TransactionConfirmationInteractorProtocol {
    func updateBalance(to value: Int64) {
        let dataModel = "Payment"
        let bundle = Constants.paymentBundle
        let id: NSNumber = 1
        let predicate = NSPredicate(format: "id == %@", id)
        let users = CoreDataManager.shared.fetchObjects(UserEntity.self, predicate: predicate, dataModel: dataModel, bundle: bundle)
        if let user = users.first {
            user.balance = value
            CoreDataManager.shared.saveContext(dataModel: dataModel, bundle: bundle)
        }
    }
    
    func getBalance() -> Int64 {
        let dataModel = "Payment"
        let bundle = Constants.paymentBundle
        let users = CoreDataManager.shared.fetchObjects(UserEntity.self, dataModel: dataModel, bundle: bundle) as [UserEntity]
        if let user = users.first {
            return user.balance
        }
        return 0
        
    }
    
    func addTransactionHistory(newTransaction: Transaction) {
        let dataModel = "Payment"
        let bundle = Constants.paymentBundle
        let transaction = CoreDataManager.shared.createObject(TransactionEntity.self, dataModel: dataModel, bundle: bundle) as TransactionEntity
        transaction.id = newTransaction.id
        transaction.bankName = newTransaction.bankName
        transaction.merchantName = newTransaction.merchantName
        transaction.price = newTransaction.price
        transaction.createdDate = Date()
        CoreDataManager.shared.saveContext(dataModel: dataModel, bundle: bundle)
    }
    
    
}
