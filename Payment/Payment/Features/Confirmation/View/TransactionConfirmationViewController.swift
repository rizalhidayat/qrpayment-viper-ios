//
//  TransactionConfirmationViewController.swift
//  Payment
//
//  Created by Rizal Hidayat on 30/03/24.
//

import UIKit
import Core

protocol TransactionConfirmationViewProtocol {
    func showCurrentTransaction(transaction: Transaction)
    func showFailed(message: String)
    func showSuccess(message: String)
}

class TransactionConfirmationViewController: UIViewController {
    @IBOutlet weak var idLabel: UILabel!
    
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var merchantNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var payButton: UIButton!
    
    var presenter: TransactionConfirmationPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initButtons()
        presenter.loadCurrentTransaction()
    }
    
    private func initButtons() {
        payButton.addTarget(self, action: #selector(pay), for: .touchUpInside)
    }
    
    @objc
    private func pay() {
        presenter.pay()
    }
}

extension TransactionConfirmationViewController: TransactionConfirmationViewProtocol {
    func showCurrentTransaction(transaction: Transaction) {
        idLabel.text = transaction.id ?? "-"
        bankNameLabel.text = transaction.bankName ?? "-"
        merchantNameLabel.text = transaction.merchantName ?? "-"
        priceLabel.text = PriceFormmatter.formatPrice(transaction.price)
    }
    
    func showFailed(message: String) {
        let alertController = UIAlertController(
            title: "Maaf",
            message: message,
            preferredStyle: .alert
        )
        let okayAction = UIAlertAction(title: "Oke", style: .cancel, handler: nil)
        alertController.addAction(okayAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showSuccess(message: String) {
        let alertController = UIAlertController(
            title: "Selamat",
            message: message,
            preferredStyle: .alert
        )
        let okayAction = UIAlertAction(title: "Oke", style: .default) { _ in
            self.presenter.backToHome()
        }
        alertController.addAction(okayAction)
        present(alertController, animated: true, completion: nil)
    }
}
