//
//  TransactionConfirmationRouter.swift
//  Payment
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation

protocol TransactionConfirmationRouterProtocol {
    func popToRoot()
}

class TransactionConfirmationRouter: TransactionConfirmationRouterProtocol {
    private weak var view: TransactionConfirmationViewController?
    
    init(view: TransactionConfirmationViewController?) {
        self.view = view
    }
    
    func popToRoot() {
        view?.navigationController?.popToRootViewController(animated: true)
    }
    
}
