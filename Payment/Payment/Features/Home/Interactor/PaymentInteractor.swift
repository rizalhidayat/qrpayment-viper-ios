//
//  HomeInteractor.swift
//  Payment
//
//  Created by Rizal Hidayat on 29/03/24.
//

import Foundation
import Core

protocol PaymentInteractorProtocol {
    func getTransactionHistories() -> [Transaction]
    func getBalance() -> Int64
    func updateBalance(to value: Int64)
}

class PaymentInteractor: PaymentInteractorProtocol {
    
    init() {
        createUser()
    }
    
    private func createUser() {
        let dataModel = "Payment"
        let bundle = Constants.paymentBundle
        let users = CoreDataManager.shared.fetchObjects(UserEntity.self, dataModel: dataModel, bundle: bundle) as [UserEntity]
        if users.count > 0 {
            return
        }
        let newUser = CoreDataManager.shared.createObject(UserEntity.self, dataModel: dataModel, bundle: bundle) as UserEntity
        newUser.id = 1
        newUser.name = "Rizal Hidayat"
        newUser.balance = 10_000_000
        CoreDataManager.shared.saveContext(dataModel: dataModel, bundle: bundle)
    }
    
    func updateBalance(to value: Int64) {
        let dataModel = "Payment"
        let bundle = Constants.paymentBundle
        let id: NSNumber = 1
        let predicate = NSPredicate(format: "id == %@", id)
        let users = CoreDataManager.shared.fetchObjects(UserEntity.self, predicate: predicate, dataModel: dataModel, bundle: bundle)
        if let user = users.first {
            user.balance = value
            CoreDataManager.shared.saveContext(dataModel: dataModel, bundle: bundle)
        }
    }
    
    func getBalance() -> Int64 {
        let dataModel = "Payment"
        let bundle = Constants.paymentBundle
        let users = CoreDataManager.shared.fetchObjects(UserEntity.self, dataModel: dataModel, bundle: bundle) as [UserEntity]
        if let user = users.first {
            return user.balance
        }
        return 0
        
    }
    
    func getTransactionHistories() -> [Transaction] {
        let transactions = CoreDataManager.shared.fetchObjects(TransactionEntity.self, dataModel: "Payment", bundle: Constants.paymentBundle) as [TransactionEntity]
        let fetchedTransactions = entityToStructMapper(data: transactions)
        return fetchedTransactions
    }
    
    private func entityToStructMapper(data: [TransactionEntity]) -> [Transaction] {
        let transactions = data.compactMap { entity in
            let transaction = Transaction(bankName: entity.bankName, id: entity.id, price: entity.price, merchantName: entity.merchantName, createdDate: entity.createdDate)
            return transaction
        }
        return transactions
    }
}
