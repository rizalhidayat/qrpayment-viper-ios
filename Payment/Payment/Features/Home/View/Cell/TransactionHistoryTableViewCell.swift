//
//  TransactionHistoryTableViewCell.swift
//  Payment
//
//  Created by Rizal Hidayat on 29/03/24.
//

import UIKit
import Core

class TransactionHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var merchantNameLabel: UILabel!
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with transaction: Transaction) {
        idLabel.text = transaction.id ?? "-"
        merchantNameLabel.text = transaction.merchantName ?? "-"
        bankNameLabel.text = transaction.bankName ?? "-"
        priceLabel.text = PriceFormmatter.formatPrice(transaction.price)
        createdDateLabel.text = transaction.createdDate?.string("dd MMM yyyy - HH:mm") ?? "-"
    }
    
}
