//
//  PaymentViewController.swift
//  Payment
//
//  Created by Rizal Hidayat on 29/03/24.
//

import UIKit
import Core

protocol PaymentViewProtocol {
    func showTransactions()
    func showBalance(_ balance: Int64)
}

class PaymentViewController: UIViewController {

    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var addBalanceButton: UIButton!
    
    var presenter: PaymentPresenterProtocol!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.loadTransactions()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        initButtons()
    }
    
    private func initTableView() {
        historyTableView.dataSource = self
        historyTableView.register(cellClass: TransactionHistoryTableViewCell.self, bundle: Constants.paymentBundle)
    }
    
    private func initButtons() {
        payButton.addTarget(self, action: #selector(openScanner), for: .touchUpInside)
        payButton.layer.cornerRadius = payButton.bounds.width / 2
        payButton.clipsToBounds = true
        addBalanceButton.addTarget(self, action: #selector(addBalance), for: .touchUpInside)
    }
    
    @objc
    private func openScanner() {
        presenter.openScanner()
    }
    
    @objc
    private func addBalance() {
        presenter.addBalance(500_000)
    }
}

extension PaymentViewController: PaymentViewProtocol {
    func showBalance(_ balance: Int64) {
        amountLabel.text = PriceFormmatter.formatPrice(balance)
    }
    
    func showTransactions() {
        historyTableView.reloadData()
    }
    
}

extension PaymentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = presenter.transactions?.count, count > 0 {
            tableView.restore()
            return count
        } else {
            tableView.setEmptyView(title: "Tidak ada data", message: "Anda tidak memiliki riwayat transaksi")
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(cellClass: TransactionHistoryTableViewCell.self, forIndexPath: indexPath)
        guard let transaction = presenter.transactions?[indexPath.row] else { return cell }
        cell.configure(with: transaction)
        return cell
    }
    
    
}


