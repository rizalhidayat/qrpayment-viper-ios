//
//  PaymentRouter.swift
//  Payment
//
//  Created by Rizal Hidayat on 29/03/24.
//

import UIKit
import Core
import AVFoundation

protocol PaymentRouterProtocol {
    func navigateToScanner()
}

class PaymentRouter {
    private weak var view: PaymentViewController?
    
    init(view: PaymentViewController?) {
        self.view = view
    }
}

extension PaymentRouter: PaymentRouterProtocol {
    func navigateToScanner() {
        checkCameraPermission()
    }
    
    private func checkCameraPermission() {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
                DispatchQueue.main.async {
                    if granted {
                        self?.pushToScannerViewController()
                    } else {
                        self?.openCameraPermissionAlert()
                    }
                }
            }
        case .restricted, .denied:
            openCameraPermissionAlert()
        case .authorized:
            pushToScannerViewController()
        @unknown default:
            break
        }
    }

    private func pushToScannerViewController() {
        let vc = ScannerViewController(nibName: String(describing: ScannerViewController.self), bundle: Constants.paymentBundle)
        vc.hidesBottomBarWhenPushed = true
        vc.title = "Scan QRIS"
        let router = ScannerRouter(view: vc)
        let presenter = ScannerPresenter(view: vc, router: router)
        vc.presenter = presenter
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func openCameraPermissionAlert() {
        let alertController = UIAlertController(
            title: "Akses Kamera Ditolak",
            message: "Mohon izinkan kamera di Pengaturan untuk mengambil foto.",
            preferredStyle: .alert
        )
        
        let settingsAction = UIAlertAction(
            title: "Pengaturan",
            style: .default) { _ in
                guard let settingsURL = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsURL) {
                    UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                }
            }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: "Batal", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        view?.present(alertController, animated: true, completion: nil)
    }
}
