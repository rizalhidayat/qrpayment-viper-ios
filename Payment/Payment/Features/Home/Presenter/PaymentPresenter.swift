//
//  PaymentPresenter.swift
//  Payment
//
//  Created by Rizal Hidayat on 29/03/24.
//

import Foundation

protocol PaymentPresenterProtocol {
    var transactions: [Transaction]? { get }
    func loadTransactions()
    func openScanner()
    func addBalance(_ value: Int64)
}

class PaymentPresenter: PaymentPresenterProtocol {
    private(set) var transactions: [Transaction]?
    
    private let view: PaymentViewProtocol
    private let router: PaymentRouterProtocol
    private let interactor: PaymentInteractorProtocol
    
    init(view: PaymentViewProtocol, router: PaymentRouterProtocol, interactor: PaymentInteractorProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
        
    func loadTransactions() {
        transactions = interactor.getTransactionHistories().reversed()
        view.showTransactions()
        let balance = interactor.getBalance()
        view.showBalance(balance)
    }
    
    func openScanner() {
        router.navigateToScanner()
    }
    
    func addBalance(_ value: Int64) {
        let currentBalance = interactor.getBalance()
        let newBalance = currentBalance + value
        interactor.updateBalance(to: newBalance)
        loadTransactions()
    }
}
