//
//  PaymentPageBuilder.swift
//  Payment
//
//  Created by Rizal Hidayat on 01/04/24.
//

import UIKit
import Core

public class PaymentPageBuilder {
    public static func makePaymentPage() -> UIViewController {
        let vc = PaymentViewController(nibName: String(describing: PaymentViewController.self), bundle: Constants.paymentBundle)
        let router = PaymentRouter(view: vc)
        let interactor = PaymentInteractor()
        let presenter = PaymentPresenter(view: vc, router: router, interactor: interactor)
        vc.presenter = presenter
        return vc
    }
}
