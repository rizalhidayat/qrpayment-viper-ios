//
//  ChartResponse.swift
//  Portofolio
//
//  Created by Rizal Hidayat on 01/04/24.
//

import Foundation

struct Chart: Decodable {
    let type: String
    let data: ChartType

    enum CodingKeys: String, CodingKey {
        case type = "type"
        case data = "data"
    }
    
    enum ChartType: Decodable {
        case lineChart(LineChartData)
        case donutChart([DonutChartData])

        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            if let x = try? container.decode([DonutChartData].self) {
                self = .donutChart(x)
                return
            }
            if let x = try? container.decode(LineChartData.self) {
                self = .lineChart(x)
                return
            }
            throw DecodingError.typeMismatch(ChartType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ChartType"))
        }
    }
}

// MARK: - DonutChartData
struct DonutChartData: Decodable {
    let label: String
    let percentage: String
    let data: [TransationDetail]

    enum CodingKeys: String, CodingKey {
        case label = "label"
        case percentage = "percentage"
        case data = "data"
    }
    
    struct TransationDetail: Decodable {
        let trxDate: String
        let nominal: Int

        enum CodingKeys: String, CodingKey {
            case trxDate = "trx_date"
            case nominal = "nominal"
        }
    }
}

// MARK: - LineChartData
struct LineChartData: Decodable {
    let month: [Int]

    enum CodingKeys: String, CodingKey {
        case month = "month"
    }
}


