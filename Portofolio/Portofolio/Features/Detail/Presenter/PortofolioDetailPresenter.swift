//
//  PortofolioDetailPresenter.swift
//  Portofolio
//
//  Created by Rizal Hidayat on 01/04/24.
//

import Foundation

protocol PortofolioDetailPresenterProtocol {
    var transactions: [DonutChartData.TransationDetail] { get }
    func loadTransactions()
}

class PortofolioDetailPresenter: PortofolioDetailPresenterProtocol {
    let transactions: [DonutChartData.TransationDetail]
    
    private let view: PortofolioDetailViewProtocol
    
    init(transactions: [DonutChartData.TransationDetail], view: PortofolioDetailViewProtocol) {
        self.transactions = transactions
        self.view = view
    }
    
    func loadTransactions() {
        view.showTransactions()
    }
}
