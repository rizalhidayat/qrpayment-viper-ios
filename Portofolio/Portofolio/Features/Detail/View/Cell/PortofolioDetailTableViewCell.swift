//
//  PortofolioDetailTableViewCell.swift
//  Portofolio
//
//  Created by Rizal Hidayat on 01/04/24.
//

import UIKit
import Core

class PortofolioDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with transaction: DonutChartData.TransationDetail) {
        dateLabel.text = transaction.trxDate
        priceLabel.text = PriceFormmatter.formatPrice(Int64(transaction.nominal))
    }
}
