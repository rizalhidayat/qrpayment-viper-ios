//
//  PortofolioDetailViewController.swift
//  Portofolio
//
//  Created by Rizal Hidayat on 01/04/24.
//

import UIKit
import Core

protocol PortofolioDetailViewProtocol {
    func showTransactions()
}

class PortofolioDetailViewController: UIViewController {

    @IBOutlet weak var transactionTableView: UITableView!
    
    var presenter: PortofolioDetailPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initTableView()
    }
    
    private func initTableView() {
        transactionTableView.dataSource = self
        transactionTableView.register(cellClass: PortofolioDetailTableViewCell.self, bundle: Constants.portofolioBundle)
    }
}

extension PortofolioDetailViewController: PortofolioDetailViewProtocol {
    func showTransactions() {
        transactionTableView.reloadData()
    }
}

extension PortofolioDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(cellClass: PortofolioDetailTableViewCell.self, forIndexPath: indexPath)
        let transaction = presenter.transactions[indexPath.row]
        cell.configure(with: transaction)
        return cell
    }
    
    
}
