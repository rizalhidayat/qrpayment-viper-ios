//
//  PortofolioInteractor.swift
//  Portofolio
//
//  Created by Rizal Hidayat on 01/04/24.
//

import Foundation
import Core

protocol PortofolioInteractorProtocol {
    func fetchLineCharts() -> LineChartData?
    func fetchDonutCharts() -> [DonutChartData]?
}

class PortofolioInteractor: PortofolioInteractorProtocol {
    private let charts: [Chart]?
    
    init() {
        self.charts = JSONManager.shared.loadAndDecodeJSON(from: "Chart", in: Constants.portofolioBundle, as: [Chart].self)
    }
    
    func fetchLineCharts() -> LineChartData? {
        guard let charts = self.charts else {
            return nil
        }
        for chart in charts {
            if case let .lineChart(data) = chart.data {
                return data
            }
        }
        return nil
    }
    
    func fetchDonutCharts() -> [DonutChartData]? {
        guard let charts = self.charts else {
            return nil
        }
        for chart in charts {
            if case let .donutChart(data) = chart.data {
                return data
            }
        }
        return nil
    }
}
