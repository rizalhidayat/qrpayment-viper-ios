//
//  PortofolioPresenter.swift
//  Portofolio
//
//  Created by Rizal Hidayat on 01/04/24.
//

import Foundation

protocol PortofolioPresenterProtocol {
    func loadCharts()
    func openDetail(title: String, transactions: [DonutChartData.TransationDetail])
}

class PortofolioPresenter: PortofolioPresenterProtocol {
    private let view: PortofolioViewProtocol
    private let interactor: PortofolioInteractorProtocol
    private let router: PortofolioRouterProtocol
    
    init(view: PortofolioViewProtocol, interactor: PortofolioInteractorProtocol, router: PortofolioRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadCharts() {
        if let donutsData = interactor.fetchDonutCharts() {
            view.showDonutChart(data: donutsData)
        }
        if let lineData = interactor.fetchLineCharts() {
            view.showLineChart(data: lineData)
        }
    }
    
    func openDetail(title: String, transactions: [DonutChartData.TransationDetail]) {
        router.navigateToDetail(title: title, transactions: transactions)
    }
}
