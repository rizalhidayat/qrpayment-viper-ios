//
//  PortofolioViewController.swift
//  Portofolio
//
//  Created by Rizal Hidayat on 01/04/24.
//

import UIKit
import Core
import DGCharts

protocol PortofolioViewProtocol {
    func showLineChart(data: LineChartData)
    func showDonutChart(data: [DonutChartData])
}

class PortofolioViewController: UIViewController {
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var pieChartView: PieChartView!
    
    var presenter: PortofolioPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLineChartView()
        initPieChartView()
        presenter.loadCharts()
    }
    
    private func initLineChartView() {
        let xAxis = lineChartView.xAxis
        xAxis.drawGridLinesEnabled = false
        xAxis.drawAxisLineEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.labelFont = UIFont.systemFont(ofSize: 8)
        xAxis.valueFormatter = ChartMonthValueFormatter()
        
        lineChartView.xAxis.axisMaximum = 12
        lineChartView.setVisibleXRangeMaximum(6)
        lineChartView.extraBottomOffset = 10.0
        lineChartView.rightAxis.enabled = false
        lineChartView.dragEnabled = true
        lineChartView.pinchZoomEnabled = false
        lineChartView.doubleTapToZoomEnabled = false
        lineChartView.setScaleEnabled(false)
        lineChartView.highlightPerTapEnabled = false
        lineChartView.highlightPerDragEnabled = false
    }
    
    private func initPieChartView() {
        pieChartView.usePercentValuesEnabled = true
        pieChartView.drawHoleEnabled = false
        pieChartView.drawCenterTextEnabled = false
        pieChartView.rotationAngle = 0
        pieChartView.rotationEnabled = false
        pieChartView.highlightPerTapEnabled = true
        pieChartView.drawHoleEnabled = true
        pieChartView.holeColor = .systemBackground
        pieChartView.delegate = self
    }
}

extension PortofolioViewController: PortofolioViewProtocol {
    func showLineChart(data: LineChartData) {
        var dataEntries: [ChartDataEntry] = []
        
        for (index, value) in data.month.enumerated() {
            let chartData = ChartDataEntry(x: Double(index), y: Double(value))
            dataEntries.append(chartData)
        }
        
        let set = LineChartDataSet(entries: dataEntries, label: "Pengeluaran Anda")
        set.setColor(.label)
        set.setCircleColor(.red)
        set.drawCircleHoleEnabled = false
        set.circleRadius = 4
        let data = DGCharts.LineChartData(dataSet: set)
        lineChartView.data = data
        lineChartView.notifyDataSetChanged()
    }
    
    func showDonutChart(data: [DonutChartData]) {
        var dataEntries: [PieChartDataEntry] = []
        
        for entry in data {
            let value = Double(entry.percentage) ?? 0.0
            let dataEntry = PieChartDataEntry(value: value, label: entry.label, data: entry)
            dataEntries.append(dataEntry)
        }
        
        let set = PieChartDataSet(entries: dataEntries, label: "")
        set.colors = ChartColorTemplates.material()
        
        let data = PieChartData(dataSet: set)
        data.setValueFormatter(ChartPercentValueFormatter())
        data.setValueFont(UIFont.systemFont(ofSize: 10))
        data.setValueTextColor(.label)
        pieChartView.data = data
    }
    
}

extension PortofolioViewController: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if let entryData = entry.data as? DonutChartData {
            presenter.openDetail(title: entryData.label, transactions: entryData.data)
        }
    }
}

class ChartPercentValueFormatter: ValueFormatter {
    func stringForValue(_ value: Double, entry: DGCharts.ChartDataEntry, dataSetIndex: Int, viewPortHandler: DGCharts.ViewPortHandler?) -> String {
        return String(format: "%.1f% %", value)
    }
}

class ChartMonthValueFormatter: NSObject, AxisValueFormatter {
    private let months = ["Jan", "Feb", "Mar", "Apr",
                          "Mei", "Jun", "Jul", "Agu",
                          "Sep", "Okt", "Nov", "Des"]
    
    func stringForValue(_ value: Double, axis: DGCharts.AxisBase?) -> String {
        return months[abs(Int(value.rounded())) % months.count]
    }
}
