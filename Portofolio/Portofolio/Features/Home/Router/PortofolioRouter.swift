//
//  PortofolioRouter.swift
//  Portofolio
//
//  Created by Rizal Hidayat on 01/04/24.
//

import UIKit
import Core

protocol PortofolioRouterProtocol {
    func navigateToDetail(title: String, transactions: [DonutChartData.TransationDetail])
}

class PortofolioRouter: PortofolioRouterProtocol {
    private weak var view: PortofolioViewController?
    
    init(view: PortofolioViewController? = nil) {
        self.view = view
    }
    
    func navigateToDetail(title: String, transactions: [DonutChartData.TransationDetail]) {
        let vc = PortofolioDetailViewController(nibName: String(describing: PortofolioDetailViewController.self), bundle: Constants.portofolioBundle)
        let presenter = PortofolioDetailPresenter(transactions: transactions, view: vc)
        vc.presenter = presenter
        vc.hidesBottomBarWhenPushed = true
        vc.title = title
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}
