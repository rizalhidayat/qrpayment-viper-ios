//
//  PortofolioPageBuilder.swift
//  Portofolio
//
//  Created by Rizal Hidayat on 01/04/24.
//

import UIKit
import Core

public class PortofolioPageBuilder {
    public static func makePortofolioPage() -> UIViewController {
        let vc = PortofolioViewController(nibName: String(describing: PortofolioViewController.self), bundle: Constants.portofolioBundle)
        let interactor = PortofolioInteractor()
        let router = PortofolioRouter(view: vc)
        let presenter = PortofolioPresenter(view: vc, interactor: interactor, router: router)
        vc.presenter = presenter
        return vc
    }
}
