//
//  MainTabBarController.swift
//  QRPaymentApp
//
//  Created by Rizal Hidayat on 29/03/24.
//

import UIKit
import Core
import Payment
import Promo
import Portofolio

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func makeNavigation(viewController: UIViewController) -> UINavigationController {
        let navigation = UINavigationController(rootViewController: viewController)
        navigation.delegate = self
        navigation.navigationBar.prefersLargeTitles = false
        return navigation
    }
    
    func makeTabBar() -> MainTabBarController {
        let paymentVC = PaymentPageBuilder.makePaymentPage()
        paymentVC.title = "Pembayaran"
        paymentVC.tabBarItem = UITabBarItem(title: "Pembayaran", image: UIImage(systemName: "newspaper"), tag: 0)
        
        let promoVC = PromoPageBuilder.makePromoPage()
        promoVC.title = "Promo"
        promoVC.tabBarItem = UITabBarItem(title: "Promo", image: UIImage(systemName: "tag"), tag: 1)
       
        let portoVC = PortofolioPageBuilder.makePortofolioPage()
        portoVC.title = "Portofolio"
        portoVC.tabBarItem = UITabBarItem(title: "Portofolio", image: UIImage(systemName: "chart.pie"), tag: 2)
        self.viewControllers = [
            makeNavigation(viewController: paymentVC),
            makeNavigation(viewController: promoVC),
            makeNavigation(viewController: portoVC)
        ]
        return self
    }
    
}


extension MainTabBarController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if #available(iOS 14.0, *) {
            viewController.navigationItem.backButtonDisplayMode = .minimal
        } else {
            viewController.navigationItem.backButtonTitle = ""
        }
    }
}
