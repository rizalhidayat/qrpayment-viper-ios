//
//  ImageViewExt.swift
//  Core
//
//  Created by Rizal Hidayat on 31/03/24.
//

import UIKit
import Kingfisher

public extension UIImageView {
    func setImage(with url: URL?) {
        guard let url = url else { return }
        let placeholderImage: UIImage? = UIImage(named: "img-placeholder")
        self.layoutIfNeeded()
        
        let height = self.bounds.size.height == 0 ? 180 : self.bounds.size.height
        let width = self.bounds.size.width == 0 ? 180 : self.bounds.size.width
        let processor = DownsamplingImageProcessor(size: CGSize(width: width, height: height))
        
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url,
                         placeholder: placeholderImage,
                         options: [
                            .processor(processor),
                            .scaleFactor(UIScreen.main.scale),
                            .cacheOriginalImage
                         ]
        )
    }
}
