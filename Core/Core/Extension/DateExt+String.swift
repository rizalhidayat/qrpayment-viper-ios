//
//  DateExt+String.swift
//  Core
//
//  Created by Rizal Hidayat on 29/03/24.
//

import Foundation

extension Date {
    public func string(_ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Constants.locale
        return dateFormatter.string(from: self)
    }
}
