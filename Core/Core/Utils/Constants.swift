//
//  Constants.swift
//  Core
//
//  Created by Rizal Hidayat on 29/03/24.
//

import Foundation

public enum Constants {
    public static let paymentBundle = Bundle(identifier: "Rizal-Hidayat.Payment")
    public static let coreBundle = Bundle(identifier: "Rizal-Hidayat.Core")
    public static let promoBundle = Bundle(identifier: "Rizal-Hidayat.Promo")
    public static let portofolioBundle = Bundle(identifier: "Rizal-Hidayat.Portofolio")
    static let locale = Locale(identifier: "en_ID")
}
