//
//  PriceFormmatter.swift
//  Core
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation

public class PriceFormmatter {
    
    public static func formatPrice(_ price: Int64) -> String? {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Constants.locale
        numberFormatter.groupingSeparator = "."
        numberFormatter.numberStyle = .currency
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 0
        let formattedPrice = numberFormatter.string(from: NSNumber(value: abs(price)))
        return formattedPrice
    }
}

