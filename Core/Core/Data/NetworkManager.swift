//
//  NetworkManager.swift
//  Core
//
//  Created by Rizal Hidayat on 30/03/24.
//

import Foundation
import Alamofire
import Combine

public class NetworkManager {
    public static let shared = NetworkManager()
    
    public init() { }
    
    private var headers: HTTPHeaders {
        var data: HTTPHeaders = [:]
        guard let path = Constants.coreBundle?.path(forResource: "NetworkHeader", ofType: "plist"),
              let headersDict = NSDictionary(contentsOfFile: path) as? [String: String] else {
            return data
        }
        for (key, value) in headersDict {
            data[key] = value
        }
        return data
    }

    public func execute<T: Decodable>(_ url: URLConvertible,
                                method: HTTPMethod = .get,
                                parameters: Parameters? = nil,
                                encoding: ParameterEncoding = URLEncoding.default) -> AnyPublisher<T, Error> {
        return Future<T, Error> { promise in
            AF.request(url,
                       method: method,
                       parameters: parameters,
                       encoding: encoding,
                       headers: self.headers)
                .validate()
                .responseDecodable(of: T.self) { response in
                    switch response.result {
                    case .success(let value):
                        promise(.success(value))
                    case .failure(let error):
                        promise(.failure(error))
                    }
                }
        }
        .eraseToAnyPublisher()
    }
}
