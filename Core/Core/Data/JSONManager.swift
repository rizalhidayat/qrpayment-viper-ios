//
//  JSONManager.swift
//  Core
//
//  Created by Rizal Hidayat on 01/04/24.
//

import Foundation

public class JSONManager {
    public static let shared = JSONManager()
    public init() { }
    
    public func loadAndDecodeJSON<T: Decodable>(from filename: String, in bundle: Bundle?, as type: T.Type) -> T? {
        guard let url = bundle?.url(forResource: filename, withExtension: "json") else {
            print("JSON file '\(filename)' not found in bundle.")
            return nil
        }
        do {
            let jsonData = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let decodedData = try decoder.decode(type, from: jsonData)
            return decodedData
        } catch {
            print("Error loading and decoding JSON:", error.localizedDescription)
            return nil
        }
    }
}
