//
//  CoreDataManager.swift
//  Core
//
//  Created by Rizal Hidayat on 29/03/24.
//
import Foundation
import CoreData

public class CoreDataManager {
    public static let shared = CoreDataManager()
    
    private var context: NSManagedObjectContext?
    private var currentDataModel: String?
    private var currentBundle: Bundle?
    
    private init() {}
    
    private func getContext(dataModel: String, bundle: Bundle?) -> NSManagedObjectContext {
        if let context,
           let currentBundle, currentBundle == bundle,
           let currentDataModel, currentDataModel == dataModel {
            return context
        } else {
            guard let modelURL = bundle?.url(forResource: dataModel, withExtension: "momd") else {
                fatalError("Failed to locate data model file in framework bundle.")
            }
            
            guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
                fatalError("Failed to initialize managed object model from: \(modelURL)")
            }
            
            let container = NSPersistentContainer(name: "Payment", managedObjectModel: managedObjectModel)
            
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                if let error = error as NSError? {
                    fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            })
            self.context = container.viewContext
            self.currentDataModel = dataModel
            self.currentBundle = bundle
            return self.context!
        }
    }
    
    // MARK: - CRUD Operations
    
    public func saveContext(dataModel: String, bundle: Bundle?) {
        let context = getContext(dataModel: dataModel, bundle: bundle)
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                fatalError("Failed to save context: \(error)")
            }
        }
    }
    
    public func createObject<T: NSManagedObject>(_ objectType: T.Type, dataModel: String, bundle: Bundle?) -> T {
        let entityName = String(describing: objectType)
        let context = getContext(dataModel: dataModel, bundle: bundle)
        guard let entity = NSEntityDescription.entity(forEntityName: entityName, in: context) else {
            fatalError("Entity description not found for \(entityName)")
        }
        return T(entity: entity, insertInto: context)
    }
    
    public func fetchObjects<T: NSManagedObject>(_ objectType: T.Type, predicate: NSPredicate? = nil, dataModel: String, bundle: Bundle?) -> [T] {
        let entityName = String(describing: objectType)
        let fetchRequest = NSFetchRequest<T>(entityName: entityName)
        fetchRequest.predicate = predicate
        do {
            return try getContext(dataModel: dataModel, bundle: bundle).fetch(fetchRequest)
        } catch {
            fatalError("Failed to fetch objects: \(error)")
        }
    }
    
    public func updateContext(dataModel: String, bundle: Bundle?) {
        saveContext(dataModel: dataModel, bundle: bundle)
    }
    
    public func deleteObject(_ object: NSManagedObject, dataModel: String, bundle: Bundle?) {
        let context = getContext(dataModel: dataModel, bundle: bundle)
        context.delete(object)
        saveContext(dataModel: dataModel, bundle: bundle)
    }
}
